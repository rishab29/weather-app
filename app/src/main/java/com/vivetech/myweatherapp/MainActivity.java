package com.vivetech.myweatherapp;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vivetech.myweatherapp.models.CurrentWeather;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG=MainActivity.class.getSimpleName();


    private CurrentWeather mcurrentWeather;

    @BindView(R.id.refreshImageView) ImageView mrefreshImageView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.tempTextView) TextView mTemperature;

    @BindView(R.id.timeTextView)
    TextView mTime;

    @BindView(R.id.humidityValueTextView)
    TextView mHumdidityValue;

    @BindView(R.id.locationTextView)
    TextView mLocation;

    @BindView(R.id.rainValueTextView)
    TextView mRainValue;

    @BindView(R.id.summaryTextView)
    TextView mSummary;

    @BindView(R.id.weatherImageView)
    ImageView mImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

       final double longitude=28.4595;
       final double latitude=77.0266;
        mProgressBar.setVisibility(View.INVISIBLE);
        mrefreshImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getForecast(longitude,latitude);
            }
        });
        getForecast(longitude,latitude);


    }

    private void getForecast(double longitude,double latitude) {
        String api_key="https://api.darksky.net/forecast/d1120f988a0690d3cfce299c8f4f652d/"+longitude+","+latitude;

        if(isNetworkAvailable()) {
            toggleRefreshView();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(api_key)
                    .build();

            Call call = client.newCall(request);
            // Response response=call.execute();// It is executing a synchronous call so avoid it.

            //This is the async call so data will be fetched and callback will be executed
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefreshView();
                        }
                    });
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                toggleRefreshView();
                            }
                        });
                        String jsonData=response.body().string();
                        Log.v(TAG,jsonData);
                        if (response.isSuccessful()) {
                             mcurrentWeather=getCurrentDetails(jsonData);
                             runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     updateDisplay();
                                 }
                             });

                        } else {
                            alertUserAboutError();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Error is " + e);
                    }
                }
            });
        }else{
            Toast.makeText(this,"Network is Unavailable", Toast.LENGTH_LONG).show();

        }
    }

    private void toggleRefreshView() {
        if(mProgressBar.getVisibility()==View.INVISIBLE) {
            mrefreshImageView.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.VISIBLE);
        }else{
            mrefreshImageView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void updateDisplay() {
        mTemperature.setText(mcurrentWeather.getTemperature() + "");
        mTime.setText("At " + mcurrentWeather.getFormattedTime() + " It Will Be");
        mHumdidityValue.setText(mcurrentWeather.getHumidity() + "");
        mLocation.setText("Gurgaon, New Delhi");
        mSummary.setText(mcurrentWeather.getSummary());
        mRainValue.setText(mcurrentWeather.getPrecipChance() + "%");

        Drawable drawable = getResources().getDrawable(mcurrentWeather.getIconId());
        mImageView.setImageDrawable(drawable);
    }


    private CurrentWeather getCurrentDetails(String jsonData) throws Exception {
        //getting the main json object
        JSONObject forecast=new JSONObject(jsonData);

        String timezone= forecast.getString("timezone");

        // getting an object present inside json object
        JSONObject currently=forecast.getJSONObject("currently");

        CurrentWeather currentweather=new CurrentWeather();
        currentweather.setHumidity(currently.getDouble("humidity"));
        currentweather.setTime(currently.getLong("time"));
        currentweather.setIcon(currently.getString("icon"));
        currentweather.setPrecipChance(currently.getDouble("precipProbability"));
        currentweather.setSummary(currently.getString("summary"));
        currentweather.setTemperature(currently.getDouble("temperature"));
        currentweather.settimeZone(timezone);
        Log.v(TAG,"Temp is"+ currentweather.getTemperature());

        return currentweather;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network=manager.getActiveNetworkInfo();
        boolean isAvailable=false;
        if(network !=null && network.isAvailable()){
            isAvailable=true;
        }
        return isAvailable;
    }

    private void alertUserAboutError(){
        AlertDialogFragment dialog=new AlertDialogFragment();
        dialog.show(getFragmentManager(),"Error_Dialog");
        Log.v(TAG,"Dialog Box Terminated");
    }




}
